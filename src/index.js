import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App.js';
import './index.css';
import'bootstrap/dist/css/bootstrap.min.css';
//const msg=<h1>MY FIRST PROGRAM</h1>

class UserDetails extends React.Component{
	constructor(props){
	 	super(props);
	 	this.state = { username: '' };
	 }
	submitEvent = (event) => {
	    event.preventDefault();
	    alert("You are submitting " + this.state.username);
  	}
  	myChangeHandler = (event) => {
    	this.setState({username: event.target.value});
  	}
	render(){
		return(
		<form>
		  <div className="form-row">
		    <div className="form-group col-md-6">
		      <label>Email</label>
		      <input type="email" className="form-control" id={email} value={this.state.value} onChange={this.handleChange} placeholder="this.state.placeholder"/>
		    </div>
		    <div className="form-group col-md-6">
		      <label>Password</label>
		      <input type="password" className="form-control" id="inputPassword4" placeholder="Password"/>
		    </div>
		  </div>
		  <div className="form-group">
		    <label>Address</label>
		    <input type="text" className="form-control" id="inputAddress" placeholder="1234 Main St"/>
		  </div>
		  <div className="form-group">
		    <label>Address 2</label>
		    <input type="text" className="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor"/>
		  </div>
		  <div className="form-row">
		    <div className="form-group col-md-6">
		      <label>City</label>
		      <input type="text" className="form-control" id="inputCity"/>
		    </div>
		    <div className="form-group col-md-4">
		      <label>State</label>
		      <select id="inputState" className="form-control">
		        <option value="">Choose...</option>
		        <option>...</option>
		      </select>
		    </div>
		    <div className="form-group col-md-2">
		      <label>Zip</label>
		      <input type="text" className="form-control" id="inputZip"/>
		    </div>
		  </div>
		  <div className="form-group">
		    <div className="form-check">
		      <input className="form-check-input" type="checkbox" id="gridCheck"/>
		      <label className="form-check-label" htmlFor="gridCheck">
		        Check me out
		      </label>
		    </div>
		  </div>
		  <button type="submit" className="btn btn-primary">Sign in</button>
		</form>
		);
	}
}

ReactDOM.render(<UserDetails/>,document.getElementById('root'));